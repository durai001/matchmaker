import multiprocessing
import pickle
from pathlib import Path
from shutil import copyfile

from matchmaker import helper, modelling_functions, sequence_feature_extraction, structure_feature_extraction
from matchmaker.helper import path_type


def get_full_c_term_sequences(sequence_file, hmm_file, subsequence_file):
    """
    Get C-terminal sequence starting with HMM sequence
    Parameters
    ----------
    sequence_file
    hmm_file
    subsequence_file

    Returns
    -------
    subsequence file
    """
    c_hmm_file = helper.get_hmm_sequences(sequence_file, hmm_file, subsequence_file)
    c_hmm_sequences = helper.get_sequences_from_fasta(c_hmm_file, prune_headers=False)
    c_hmm_sequences = {k.split("/")[0]: v for k, v in c_hmm_sequences.items()}
    sequences = helper.get_sequences_from_fasta(sequence_file, prune_headers=False)
    with open(subsequence_file, "w") as f:
        for key, sequence in c_hmm_sequences.items():
            if key not in sequences:
                print(key, sequences.keys())
            f.write(
                f">{key}\n{sequences[key][sequences[key].find(sequence.upper()):]}\n"
            )
    return subsequence_file


def make_template_subsequences(template_dir: path_type, hmm_file: path_type):
    """
    Extract HMM sequences from template PDB files

    Parameters
    ----------
    template_dir
    hmm_file

    Returns
    -------
    subsequence file (template_dir / templates_c.fasta)
    """
    template_dir = Path(template_dir)
    sequence_file = template_dir / "templates.fasta"
    helper.get_sequences_from_pdb_files(template_dir.glob("*.pdb"), sequence_file)
    subsequence_file = template_dir / "templates_c.fasta"
    return get_full_c_term_sequences(sequence_file, hmm_file, subsequence_file)


def align_templates_to_sequence(
    sequence_file: path_type,
    templates_file: path_type,
    alignment_folder: path_type,
    hmm_file: path_type = None,
) -> path_type:
    """
    clustalo align sequence to template sequences

    Parameters
    ----------
    sequence_file
        file containing a single sequence
    templates_file
        subsequence file of templates
    alignment_folder
        where to store the output file
    hmm_file
        use hmm to align

    Returns
    -------
    alignment filename
    """
    _, sequence_name, _ = helper.get_file_parts(sequence_file)
    _, templates_name, _ = helper.get_file_parts(templates_file)
    sequences = helper.get_sequences_from_fasta(sequence_file)
    template_sequences = helper.get_sequences_from_fasta(templates_file)
    unaligned_file = Path(alignment_folder) / f"{sequence_name}_{templates_name}.fasta"
    with open(unaligned_file, "w") as f:
        for key in sequences:
            f.write(f">{key}\n{sequences[key]}\n")
        for key in template_sequences:
            f.write(f">{key}\n{template_sequences[key]}\n")
    aligned_file = (
        Path(alignment_folder) / f"{sequence_name}_{templates_name}_aln.fasta"
    )
    helper.clustal_msa_from_sequences(
        str(unaligned_file), aligned_file, input_hmm_file=hmm_file
    )
    return aligned_file


def convert_fasta_to_ali(
    sequence_name: str,
    fasta_file: path_type,
    templates_start_end: dict,
    ali_file: path_type = None,
) -> path_type:
    """
    Converts fasta of sequence and templates to ali format

    Parameters
    ----------
    sequence_name
        name of sequence, rest are assumed as template structures
    fasta_file
    ali_file
    templates_start_end
        dict with start and end residue identifiers and chains for each template

    Returns
    -------
    ali filename
    """
    if ali_file is None:
        path, name, _ = helper.get_file_parts(fasta_file)
        ali_file = Path(path) / f"{name}.ali"
    sequences = helper.get_sequences_from_fasta(fasta_file)
    with open(ali_file, "w") as f:
        for key in sequences:
            if key == sequence_name:
                f.write(
                    f">P1;{key}\nsequence:{key}:::::::0.00: 0.00\n{sequences[key].upper()}*\n"
                )
            else:
                start_chain, start_pos, end_chain, end_pos = templates_start_end[key]
                f.write(
                    f">P1;{key}\nstructureX:{key}:{start_pos}:{start_chain}:{end_pos}:{end_chain}:::0.00: 0.00\n{sequences[key].upper()}*\n"
                )
    return ali_file


def make_ali_files(
    sequence_file: path_type,
    template_dir: path_type,
    hmm_file: path_type,
    ali_dir: path_type,
    c_only=True,
):
    """
    Given a fasta file with sequences, for each sequence
    make an ali file aligning the sequence to the templates
    in template_dir / templates_c.fasta using hmm_file
    ali file is stored in ali_dir as {key}.ali

    ali_dir / keys_{name}.txt contains the names of all the
    keys in sequence_file where name is the root name of sequence_file.
    This key_file can then be used as input to parallel_model.py

    Parameters
    ----------
    sequence_file
    template_dir
    hmm_file
    ali_dir
    c_only
        If True, uses C-term domain only

    Returns
    -------
    key_file
    """
    if c_only:
        template_file = template_dir / "templates_c.fasta"
    else:
        template_file = template_dir / "templates.fasta"
    template_start_end, _ = helper.get_start_end_residues(
        template_dir.glob("*.pdb"), template_file
    )
    directory, name, _ = helper.get_file_parts(sequence_file)
    directory = Path(directory)
    if c_only:
        subsequence_file = directory / f"{name}_c.fasta"
    else:
        subsequence_file = directory / f"{name}.fasta"
    key_file = ali_dir / f"keys_{name}.txt"
    if c_only:
        get_full_c_term_sequences(sequence_file, hmm_file, subsequence_file)
    with open(key_file, "w") as f1:
        for (key, sequence) in helper.get_sequences_from_fasta_yield(subsequence_file):
            f1.write(f"{key}\n")
            single_sequence_file = ali_dir / f"{key}.fasta"
            with open(single_sequence_file, "w") as f:
                f.write(f">{key}\n{sequence}\n")
            aln_fasta_file = align_templates_to_sequence(
                single_sequence_file, template_file, ali_dir, hmm_file=hmm_file
            )
            convert_fasta_to_ali(key, aln_fasta_file, template_start_end)
    return key_file


def copy_best_models(
    input_model_dir: path_type,
    output_model_dir: path_type,
    num_best: int = 10,
    ignore_none=True,
):
    """
    Copies the top num_best models (based on their NDOPE scores) of each accession in input_model_dir to output_model_dir

    Parameters
    ----------
    input_model_dir
    output_model_dir
    num_best
    ignore_none
        if True, ignores PDB files without NDOPE information
        if False, calculates NDOPE on these files by running MODELLER
    """
    log_files = Path(input_model_dir).glob("*.log")
    for log_file in log_files:
        _, name, _ = helper.get_file_parts(log_file)
        output_log_file = Path(output_model_dir) / f"{name}.log"
        if not output_log_file.exists():
            copyfile(log_file, output_log_file)
            filenames, scores, sorted_scores = model.parse_log_file(
                input_model_dir, log_file, ignore_none=ignore_none
            )
            for i in range(num_best):
                copyfile(
                    Path(input_model_dir) / f"{filenames[sorted_scores[i]]}.pdb",
                    Path(output_model_dir) / f"{name}_{i}.pdb",
                )


def extract_sequence_features_one(
    key,
    sequence,
    ali_dir: path_type,
    feature_dir: path_type,
    pssm_dir: path_type,
    scratch_data: dict,
    num_threads=5,
    overwrite=False,
):
    ali_dir = Path(ali_dir)
    feature_dir = Path(feature_dir)
    pssm_dir = Path(pssm_dir)
    single_sequence_file = ali_dir / f"{key}.fasta"
    if not single_sequence_file.exists():
        with open(single_sequence_file, "w") as f:
            f.write(f">{key}\n{sequence}\n")
    if (not (feature_dir / f"{key}_sequence.pkl").exists()) or overwrite:
        pssm_features = sequence_feature_extraction.get_pssm_features(
            single_sequence_file, pssm_dir, num_threads=num_threads, overwrite=overwrite
        )
        amino_acids = sequence_feature_extraction.get_amino_acids(single_sequence_file)
        sequence_features = {**pssm_features, **scratch_data, **amino_acids}
        pickle.dump(sequence_features, open(feature_dir / f"{key}_sequence.pkl", "wb"))


def extract_sequence_features(
    sequence_file: path_type,
    ali_dir: path_type,
    feature_dir: path_type,
    pssm_dir: path_type,
    scratch_dir: path_type,
    num_main_threads=8,
    num_threads=5,
    overwrite=False,
):
    """
    Extract sequence features for all keys in sequence_file

    Parameters
    ----------
    sequence_file
    ali_dir
    feature_dir
        feature files stored in feature_dir / {key}_sequence.pkl
    pssm_dir
    scratch_dir
    num_main_threads
    num_threads
    overwrite
    """
    ali_dir = Path(ali_dir)
    feature_dir = Path(feature_dir)
    pssm_dir = Path(pssm_dir)
    scratch_dir = Path(scratch_dir)
    predicted_ss_acc = sequence_feature_extraction.get_predicted_ss_acc(
        sequence_file, scratch_dir
    )
    print("Scratch done")
    with multiprocessing.Pool(processes=num_main_threads) as pool:
        pool.starmap(
            extract_sequence_features_one,
            [
                (
                    key,
                    sequence,
                    ali_dir,
                    feature_dir,
                    pssm_dir,
                    predicted_ss_acc[key],
                    num_threads,
                    overwrite,
                )
                for key, sequence in helper.get_sequences_from_fasta_yield(
                    sequence_file
                )
                if "X" not in sequence
            ],
        )


def extract_structure_features_one(
    key,
    model_dir: path_type,
    feature_dir: path_type,
    es_dir: path_type,
    dssp_dir: path_type,
    num_models: int = 5,
    overwrite=False,
):
    print(key)
    model_dir = Path(model_dir)
    feature_dir = Path(feature_dir)
    es_dir = Path(es_dir)
    dssp_dir = Path(dssp_dir)
    for m in range(num_models):
        pkl_file = feature_dir / f"{key}_{m}.pkl"
        if not pkl_file.exists() or overwrite:
            pdb_file = str(model_dir / f"{key}_{m}.pdb")
            if Path(pdb_file).exists():
                structure_features = structure_feature_extraction.extract_structure_features(
                    pdb_file, es_dir, dssp_dir, overwrite=overwrite
                )
                pickle.dump(structure_features, open(pkl_file, "wb"))
            else:
                continue


def extract_structure_features(
    sequence_file: path_type,
    model_dir: path_type,
    feature_dir: path_type,
    es_dir: path_type,
    dssp_dir: path_type,
    num_models: int = 3,
    overwrite: bool = False,
    num_threads: int = 20,
):
    """
    ALWAYS DO "export OMP_NUM_THREADS=2" FIRST!
    Extract structure features for all keys in sequence_file
    Takes top {num_models} models from model_dir

    Parameters
    ----------
    sequence_file
    model_dir
    feature_dir
        feature files stored in feature_dir / {key}_{m}.pkl
        where {m} is the model number
    es_dir
    dssp_dir
    num_models
    overwrite
    num_threads

    Returns
    -------

    """

    # template_info = structure_feature_extraction.get_templates_coordinate_information(template_dir)

    with multiprocessing.Pool(processes=num_threads) as pool:
        pool.starmap(
            extract_structure_features_one,
            [
                (key, model_dir, feature_dir, es_dir, dssp_dir, num_models, overwrite)
                for key, sequence in helper.get_sequences_from_fasta_yield(
                    sequence_file
                )
                if "X" not in sequence and (Path(model_dir) / f"{key}_0.pdb").exists()
            ],
        )
