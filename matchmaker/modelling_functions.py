import sys
from pathlib import Path
import modeller
from modeller.automodel import automodel, dopehr_loopmodel, refine
import numpy as np
import typing

path_type = typing.Union[str, Path]
MODELLER_LIB = "/path/to/modeller/lib/modeller-9.22/modlib"  # Replace with path to modlib (miniconda3/lib/modeller-9.20/modlib)


def make_env(pdb_directory: path_type, rand_seed: int = 5):
    """
    Make a modeller environ with a random seed.
    Change seed if parallelizing.

    Parameters
    ----------
    pdb_directory
    rand_seed

    Returns
    -------
    environ

    """
    env = modeller.environ(rand_seed=rand_seed)
    env.io.atom_files_directory = [str(pdb_directory)]
    env.io.hetatm = True
    return env


def get_ndope_score(pdb_file: path_type) -> float:
    """
    Retrieves n-DOPE score from PDB file

    Parameters
    ----------
    pdb_file

    Returns
    -------
    score
    """
    with open(pdb_file) as f:
        for line in f:
            if "Normalized DOPE score" in line:
                score = line.split(":")[-1].strip()
                return float(score)


def get_ndope_score_expensive(model_file):
    """
    Get NDOPE score by running MODELLER assess_normalized_dope()

    Parameters
    ----------
    model_file

    Returns
    -------
    NDOPE
    """
    model_file = str(model_file)
    env = modeller.environ()
    env.libs.topology.read(file=f"{MODELLER_LIB}/top_heav.lib")  # read topology
    env.libs.parameters.read(file=f"{MODELLER_LIB}/par.lib")  # read parameters
    mdl = modeller.scripts.complete_pdb(env, model_file)
    return mdl.assess_normalized_dope()


def get_ndope_profile(model_file):
    """
    Get NDOPE per residue

    Parameters
    ----------
    model_file

    Returns
    -------
    NDOPE per residue
    """
    env = modeller.environ()
    env.libs.topology.read(file=f"{MODELLER_LIB}/top_heav.lib")  # read topology
    env.libs.parameters.read(file=f"{MODELLER_LIB}/par.lib")  # read parameters
    mdl = modeller.scripts.complete_pdb(env, str(model_file))
    return mdl.get_normalized_dope_profile()


def parse_log_file(input_model_dir: path_type, log_file: path_type, ignore_none=True):
    """
    Parses a log file to get the NDOPE scores of each pdb file
    If the score is None, it tries to get te score directly from the PDB file
    If this doesn't work and ignore_none is False
        then it runs MODELLER on the PDB file to get the NDOPE scores (this takes longer)
    Parameters
    ----------
    input_model_dir
    log_file
    ignore_none

    Returns
    -------

    """
    filenames, scores = [], []
    with open(log_file) as f:
        for line in f:
            name, score = line.strip().split("\t")
            if score == "None":
                filename = Path(input_model_dir) / f"{name}.pdb"
                score = get_ndope_score(filename)
                if score is None:
                    if ignore_none:
                        continue
                    else:
                        try:
                            score = get_ndope_score_expensive(filename)
                            with open(filename) as f1:
                                lines = f1.readlines()
                            with open(filename, "w") as f1:
                                for i, l in enumerate(lines):
                                    if i == 2:
                                        f1.write(
                                            f"REMARK   6 Normalized DOPE score: {score}\n"
                                        )
                                    f1.write(l)
                        except modeller.FileFormatError:
                            continue
            filenames.append(name)
            scores.append(float(score))
    sorted_scores = np.argsort(scores)
    return filenames, scores, sorted_scores


def model(
    name: str,
    templates: list,
    ali_file: path_type,
    pdb_dir: path_type,
    model_path: path_type,
    rand_seed: int,
    num_models: int = 500,
    loop_refine: bool = False,
):
    """
    Makes models for name.
    DELETES all intermediate files
    MOVES final model pdbs and log file to model_path

    Parameters
    ----------
    name
    templates
    ali_file
    model_path
    pdb_dir
    rand_seed
    num_models
    loop_refine
    """
    env = make_env(pdb_dir, rand_seed)
    if loop_refine:
        a = dopehr_loopmodel(
            env,
            alnfile=str(ali_file),
            knowns=tuple(templates),
            sequence=name,
            assess_methods=(
                modeller.automodel.assess.DOPE,
                modeller.automodel.assess.normalized_dope,
            ),
        )
        a.md_level = None
        a.loop.starting_model = 1
        a.loop.ending_model = 5
        a.loop.md_level = refine.fast
    else:
        a = automodel(
            env,
            alnfile=str(ali_file),
            knowns=tuple(templates),
            sequence=name,
            assess_methods=(
                modeller.automodel.assess.DOPE,
                modeller.automodel.assess.normalized_dope,
            ),
        )
    a.starting_model = 1
    a.ending_model = num_models
    a.make()
    files = Path.cwd().glob(name + ".*")
    log_file = Path(model_path) / f"{name}.log"
    with open(log_file, "w") as f:
        for file_path in files:
            if file_path.suffix == ".pdb":
                f.write(f"{file_path.stem}\t{get_ndope_score(file_path)}\n")
                new_file = Path(model_path) / f"{file_path.stem}{file_path.suffix}"
                file_path.rename(new_file)
            else:
                file_path.unlink()


def main():
    """
    Makes homology models for a single key
    Takes command line arguments
    key, index, template_dir, ali_dir, model_dir
    RUN USING parallel_functions.py

    """
    arguments = sys.argv
    assert len(arguments) == 6
    key, index, template_dir, ali_dir, model_dir = arguments[1:]
    template_dir = Path(template_dir)
    ali_dir = Path(ali_dir)
    model_dir = Path(model_dir)
    templates = [filename.stem for filename in template_dir.glob("*.pdb")]
    ali_file = ali_dir / f"{key}_templates_full_c_term_aln.ali"
    print("key")
    if not ali_file.exists():
        print(f"{ali_file} missing")
    if ali_file.exists() and not (model_dir / f"{key}.log").exists():
        model(
            key,
            templates,
            ali_file,
            template_dir,
            model_dir,
            int(index),
            num_models=250,
            loop_refine=False,
        )


if __name__ == "__main__":
    main()
