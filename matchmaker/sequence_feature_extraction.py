import subprocess
from pathlib import Path

import numpy as np

from matchmaker import helper
from matchmaker.helper import path_type

SCRATCH_SH = "/path/to/scratch/SCRATCH-1D_1.1/bin/run_SCRATCH-1D_predictors.sh"
SSPRO8_VALUES = "HGIEBTSC"
ACCPRO_VALUES = "be-"
SSPRO_VALUES = "HEC"
AA = "ACDEFGHIKLMNPQRSTVWY-X"
STRING_MAPPINGS = {
    "sspro": dict(zip(SSPRO_VALUES, range(len(SSPRO_VALUES)))),
    "sspro8": dict(zip(SSPRO8_VALUES, range(len(SSPRO8_VALUES)))),
    "accpro": dict(zip(ACCPRO_VALUES, range(len(ACCPRO_VALUES)))),
    "amino_acids": dict(zip(AA, range(len(AA)))),
}


def run_psiblast(
    input_file: path_type,
    db: str,
    output_dir: path_type = None,
    n_iterations: int = 2,
    num_threads: int = 20,
):
    """
    Run psiblast on protein with specified parameters.
    Saves output as .dat
    Saves pssm info as .pssm
    Saves ascii pssm table info as .mat

    Parameters
    ----------
    input_file
    db
    output_dir
    n_iterations
    num_threads

    Returns
    -------
    success (0) or failure (-1)
    """
    path, name, _ = helper.get_file_parts(input_file)
    if output_dir is None:
        output_dir = path
    output_file = str(Path(output_dir) / name)
    command_string = (
        f"psiblast "
        f"-query {str(input_file)} "
        f"-db {db} "
        f"-out {output_file}.dat "
        f"-num_iterations {n_iterations} "
        f"-out_ascii_pssm {output_file}.mat_ "
        f"-num_threads {num_threads}"
    )
    if subprocess.check_call(command_string, shell=True) == 0:
        correct_mat_file(f"{output_file}.mat_", f"{output_file}.mat")
        Path(f"{output_file}.mat_").unlink()
        return 0
    else:
        print("PSIBLAST Failed")
        return -1


def correct_mat_file(input_file, output_file):
    """
    Corrects .mat file into readable format.

    Parameters
    ----------
    input_file
    output_file
    """
    output_handle = open(output_file, "w")
    for line in open(input_file, "r"):
        parts = line.split()
        if len(parts) and parts[0].isdigit():
            if len(parts) < 44:
                pssm = " ".join(split_string_into_chunks(line[9:69], size=3))
                line = line[:9] + pssm + line[69:]
        output_handle.write(line)
    output_handle.close()


def split_string_into_chunks(string, size=3):
    return [(string[i : i + size]).strip() for i in range(0, len(string), size)]


def parse_pssm_file(input_file: path_type) -> tuple:
    """
    Returns pssm psfm and info

    Parameters
    ----------
    input_file

    Returns
    -------
    (pssm, psfm, info)
    """
    pssm = []
    psfm = []
    info = []
    try:
        for line in open(input_file, "r"):
            line = line.split()
            if len(line) and line[0].isdigit():
                pssm_line = [float(i) for i in line[2:22]]
                pssm.append(pssm_line)
                psfm_line = [float(i) / 100.0 for i in line[22:42]]
                psfm.append(psfm_line)
                info.append(float(line[42]))
        parsed_file = (np.array(pssm).T, np.array(psfm).T, np.array(info))
    except IOError as e:
        print(e)
        parsed_file = None
    return parsed_file


def extract_sequence_features(
    sequence_file: path_type,
    pssm_dir: path_type,
    predicted_ss_acc: dict,
    num_threads: int = 30,
    overwrite: bool = False,
):
    """
    Extract sequence features from a fasta file

    Parameters
    ----------
    sequence_file
    pssm_dir
        aux dir to store pssm files
    predicted_ss_acc
    num_threads
    overwrite
        if True, reruns even if files exist

    Returns
    -------
    dict of
        pssm, psfm, info (see `get_pssm_features`)
        sspro, sspro8, accpro, accpro20 (see `get_predicted_ss_acc`)
        amino_acids
    """
    pssm_features = get_pssm_features(
        sequence_file, pssm_dir, num_threads=num_threads, overwrite=overwrite
    )
    amino_acids = get_amino_acids(sequence_file)
    return {**pssm_features, **predicted_ss_acc, **amino_acids}


def get_pssm_features(
    sequence_file: path_type,
    pssm_dir: path_type,
    num_threads: int = 20,
    overwrite: bool = False,
):
    """
    Get pssm, psfm, info by running psiblast

    Parameters
    ----------
    sequence_file
    pssm_dir
    num_threads
    overwrite

    Returns
    -------
    dict of pssm, psfm, info
    """
    _, name, _ = helper.get_file_parts(sequence_file)
    pssm_file = Path(pssm_dir) / f"{name}.mat"
    if overwrite or not Path(pssm_file).exists():
        run_psiblast(
            str(sequence_file), output_dir=str(pssm_dir), num_threads=num_threads
        )
    pssm, psfm, info = parse_pssm_file(str(pssm_file))
    return {"pssm": pssm, "psfm": psfm, "info": info}


def get_predicted_ss_acc(
    sequence_file: path_type,
    scratch_dir: path_type,
    num_threads: int = 20,
    overwrite: bool = False,
):
    """
    Get predicted secondary structure and predicted accessibility

    Parameters
    ----------
    sequence_file
    scratch_dir
    num_threads
    overwrite

    Returns
    -------
    dict of sspro, sspro8, accpro, accpro20
    """
    _, filename, _ = helper.get_file_parts(sequence_file)
    outname = Path(scratch_dir) / filename
    if not Path(f"{outname}.ss").exists() or overwrite:
        subprocess.check_call(
            f"{SCRATCH_SH} {sequence_file} {outname} {num_threads}", shell=True
        )
    data = {}
    for name, _ in helper.get_sequences_from_fasta_yield(sequence_file):
        data[name] = {
            "sspro": helper.get_sequences_from_fasta(f"{outname}.ss")[name],
            "sspro8": helper.get_sequences_from_fasta(f"{outname}.ss8")[name],
            "accpro": helper.get_sequences_from_fasta(f"{outname}.acc")[name],
            "accpro20": list(
                map(
                    int,
                    helper.get_sequences_from_fasta(f"{outname}.acc20")[name]
                    .strip()
                    .split(),
                )
            ),
        }
    return data


def get_amino_acids(sequence_file: path_type):
    """
    Gets integer-encoded amino acids

    Parameters
    ----------
    sequence_file

    Returns
    -------
    dict of amino_acids
    """
    sequence = next(helper.get_sequences_from_fasta_yield(sequence_file))[1]
    amino_acids = np.array(
        [STRING_MAPPINGS["amino_acids"][a] for a in sequence.upper()]
    )
    return {"amino_acids": amino_acids}
