from pathlib import Path

import numpy as np
import prody as pd
from Bio.PDB.ResidueDepth import get_surface, residue_depth, ca_depth, min_dist
from geometricus import MomentInvariants, SplitType

from matchmaker import helper
from matchmaker.helper import path_type


def extract_structure_features(
    pdb_file: path_type, es_dir: path_type, dssp_dir: path_type, overwrite=False
):
    """
    Extract structural features from a pdb_file

    Parameters
    ----------
    pdb_file
    es_dir
        directory to store electrostatics files
    dssp_dir
        directory to store dssp files

    Returns
    -------
    dict of
        fluctuations/perturbations _ anm/gnm _ ca/cb/mean #/min/max# (see `get_dynamics`)
        born/coulomb _ Energy/x-force/y-force/z-force _ ca/cb/mean #/min/max# (see `get_electrostatics`)
        NH_O_1/NH_O2/O_NH_1/O_NH_2 _ index, NH_O_1/NH_O2/O_NH_1/O_NH_2 _ energy, acc, alpha, kappa, phi, psi, tco (see `get_dssp_features`)
        depth _ ca/cb/mean #/min/max# (see `get_residue_depths`)
    """
    dssp_dir = str(dssp_dir)
    es_dir = str(es_dir)
    pdb_file = str(pdb_file)
    pd.confProDy(verbosity="error")
    protein = helper.load_protein_prody(pdb_file).select("protein").toAtomGroup()
    dynamics = get_dynamics(protein)
    electrostatics = get_electrostatics(protein, pdb_file, es_dir, overwrite=overwrite)
    new_pdb_file = str(Path(dssp_dir) / f"{Path(pdb_file).stem}.pdb")
    pd.writePDB(new_pdb_file, protein)
    dssp_features = get_dssp_features(protein, new_pdb_file, dssp_dir)
    residue_depths = get_residue_depths(new_pdb_file)
    invariants = get_invariants(protein)
    _, protein_residues, _, protein_sequence, _ = helper.read_pdb(new_pdb_file)
    features = {
        **dynamics,
        **electrostatics,
        **dssp_features,
        **residue_depths,
        **invariants,
    }
    for feature in features:
        if features[feature].shape[0] != len(protein_residues):
            print(feature, features[feature].shape[0], len(protein_residues))
        assert features[feature].shape[0] == len(protein_residues)
    return features


def get_invariants(protein: pd.AtomGroup, kmer_size=16, radius=10) -> dict:
    data = {
        "moments_kmer": MomentInvariants.from_prody_atomgroup(
            "n1", protein, split_type=SplitType.KMER, split_size=kmer_size
        ).moments,
        "moments_radius": MomentInvariants.from_prody_atomgroup(
            "n1", protein, split_type=SplitType.RADIUS, split_size=radius
        ).moments,
    }
    return data


def get_dynamics(protein: pd.AtomGroup, n_modes=50) -> dict:
    """
    Gets ANM and GNM fluctuations and perturbation responses for a given protein
    Parameters
    ----------
    protein
    n_modes
        Number of normal modes to calculate

    Returns
    -------
    dict of fluctuations/perturbations _ anm/gnm _ ca/cb/mean/ #min/max#
    """
    residue_splits = helper.group_indices(protein.getResindices())
    protein_anm, _ = pd.calcANM(protein, n_modes=n_modes, selstr="all")
    protein_gnm, _ = pd.calcGNM(protein, n_modes=n_modes, selstr="all")
    dynamics = {
        "fluctuation_anm": pd.calcSqFlucts(protein_anm),
        "fluctuation_gnm": pd.calcSqFlucts(protein_gnm),
    }
    data = {}
    for value_type in ["fluctuation_anm", "fluctuation_gnm"]:
        data[f"{value_type}_ca"] = np.array(
            [dynamics[value_type][index] for index in helper.get_alpha_indices(protein)]
        )
        data[f"{value_type}_cb"] = np.array(
            [dynamics[value_type][index] for index in helper.get_beta_indices(protein)]
        )
        data[f"{value_type}_mean"] = np.array(
            [
                np.nanmean([dynamics[value_type][x] for x in split])
                for split in residue_splits
            ]
        )
        # for name, func in zip(["mean", "min", "max"], [np.nanmean, np.nanmin, np.nanmax]):
        #     data[f"{value_type}_{name}"] = np.array(
        #         [func([dynamics[value_type][x] for x in split]) for split in residue_splits])
    return data


def _run_electrostatics(pdb_file, es_dir: str, es_type: str, overwrite=False) -> tuple:
    """
    Run apbs-born / apbs-coulomb on protein

    NOTE: born is 0-indexed and coulomb is 1-indexed

    Parameters
    ----------
    pdb_file
    es_dir
    es_type
    overwrite

    Returns
    -------
    (es_file, pqr_file, add)
    """
    assert es_type == "born" or es_type == "coulomb"
    _, name, _ = helper.get_file_parts(pdb_file)
    pqr_file = Path(es_dir) / f"{name}.pqr"
    if not pqr_file.exists():
        helper.pdb2pqr(pdb_file, pqr_file)
    es_file = Path(es_dir) / f"{name}_{es_type}.txt"
    if es_type == "born":
        add = 0
        if overwrite or not es_file.exists():
            helper.apbs_born(str(pqr_file), str(es_file))
    else:
        add = 1
        if overwrite or not es_file.exists():
            helper.apbs_coulomb(str(pqr_file), str(es_file))
    return es_file, pqr_file, add


def _get_electrostatics(
    protein: pd.AtomGroup,
    pdb_file: str,
    es_dir: str,
    es_type: str = "born",
    overwrite=False,
):
    """
    Run apbs-born / apbs-coulomb on protein

    NOTE: born is 0-indexed and coulomb is 1-indexed

    Parameters
    ----------
    protein
    pdb_file
    es_dir
    es_type
    overwrite

    Returns
    -------
    dict of born/coulomb _ Energy/x-force/y-force/z-force _ ca/cb/mean/ #min/max#
    """
    if not Path(pdb_file).exists():
        pd.writePDB(pdb_file, protein)
    es_file, pqr_file, add = _run_electrostatics(pdb_file, es_dir, es_type, overwrite)
    pqr_protein = pd.parsePQR(str(pqr_file))
    residue_splits = helper.group_indices(pqr_protein.getResindices())
    values, value_types = helper.parse_electrostatics_file(es_file)
    data = {}
    for value_type in value_types:
        data[f"{es_type}_{value_type}_ca"] = np.array(
            [
                values[index + add][value_type]
                if value_type in values[index + add]
                else 0
                for index in helper.get_alpha_indices(pqr_protein)
            ]
        )
        data[f"{es_type}_{value_type}_cb"] = np.array(
            [
                values[index + add][value_type]
                if value_type in values[index + add]
                else 0
                for index in helper.get_beta_indices(pqr_protein)
            ]
        )
        data[f"{es_type}_{value_type}_mean"] = np.array(
            [
                np.nanmean(
                    [
                        values[x + add][value_type]
                        for x in split
                        if (x + add) in values and value_type in values[x + add]
                    ]
                )
                for split in residue_splits
            ]
        )
    return data


def get_electrostatics(
    protein: pd.AtomGroup, pdb_file: str, es_dir: str, overwrite=False
):
    """
    Gets born and coulomb electrostatics for given protein

    Parameters
    ----------
    protein
    pdb_file
    es_dir
    overwrite

    Returns
    -------
    dict of born/coulomb _ Energy/x-force/y-force/z-force _ ca/cb/mean/min/max
    """
    es_dir = str(es_dir)
    pdb_file = str(pdb_file)
    data_born = _get_electrostatics(
        protein, pdb_file, es_dir, es_type="born", overwrite=overwrite
    )
    data_coulomb = _get_electrostatics(
        protein, pdb_file, es_dir, es_type="coulomb", overwrite=overwrite
    )
    return {**data_born, **data_coulomb}


def get_dssp_features(protein: pd.AtomGroup, pdb_file: str, dssp_dir: str):
    """
    Gets dssp features

    Parameters
    ----------
    protein
    pdb_file
    dssp_dir

    Returns
    -------
    dict of dssp_
    NH_O_1_index, NH_O_1_energy
        hydrogen bonds; e.g. -3,-1.4 means: if this residue is residue i then N-H of I is h-bonded to C=O of I-3 with an
        electrostatic H-bond energy of -1.4 kcal/mol. There are two columns for each type of H-bond, to allow for bifurcated H-bonds.
    NH_O_2_index, NH_O_2_energy
    O_NH_1_index, O_NH_1_energy
    O_NH_2_index, O_NH_2_energy
    acc
        number of water molecules in contact with this residue *10. or residue water exposed surface in Angstrom^2.
    alpha
        virtual torsion angle (dihedral angle) defined by the four Cα atoms of residues I-1,I,I+1,I+2.Used to define chirality.
    kappa
        virtual bond angle (bend angle) defined by the three Cα atoms of residues I-2,I,I+2. Used to define bend (structure ‘S’).
    phi
        IUPAC peptide backbone torsion angles.
    psi
        IUPAC peptide backbone torsion angles.
    tco
        cosine of angle between C=O of residue I and C=O of residue I-1. For α-helices, TCO is near +1, for β-sheets TCO is near -1.

    Ignores:
    dssp_bp1, dssp_bp2, and dssp_sheet_label: residue number of first and second bridge partner followed by one letter sheet label
    """
    _, name, _ = helper.get_file_parts(pdb_file)
    dssp_file = pd.execDSSP(str(pdb_file), outputname=name, outputdir=str(dssp_dir))
    protein = pd.parseDSSP(dssp=dssp_file, ag=protein, parseall=True)

    dssp_ignore = ["dssp_bp1", "dssp_bp2", "dssp_sheet_label", "dssp_resnum"]
    dssp_labels = [
        label
        for label in protein.getDataLabels()
        if label.startswith("dssp") and label not in dssp_ignore
    ]
    data = {}
    residue_splits = helper.group_indices(protein.getResindices())
    indices = [
        protein[x].getData("dssp_resnum") for x in helper.get_alpha_indices(protein)
    ]
    for label in dssp_labels:
        label_to_index = {
            i - 1: protein[x].getData(label)
            for i, x in zip(indices, helper.get_alpha_indices(protein))
        }
        data[f"{label}"] = np.array(
            [
                label_to_index[i] if i in label_to_index else 0
                for i in range(len(residue_splits))
            ]
        )
    return data


def get_residue_depths(pdb_file):
    """
    Get residue depths

    Parameters
    ----------
    pdb_file

    Returns
    -------
    dict of depth _ ca/cb/mean #/min/max#
    """
    structure, residues, _, _, _ = helper.read_pdb(pdb_file)
    surface = get_surface(structure)
    data = {
        "depth_mean": np.array(
            [residue_depth(residue, surface) for residue in residues]
        ),
        "depth_cb": np.array(
            [
                min_dist(helper.get_beta_coordinates(residue), surface)
                for residue in residues
            ]
        ),
        "depth_ca": np.array([ca_depth(residue, surface) for residue in residues]),
    }
    return data
