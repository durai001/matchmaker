from ete3 import NCBITaxa
from collections import defaultdict

NCBI = NCBITaxa()


def split_clades(identifiers, lineage_mapping):
    clade_mapping = {}
    for k in identifiers:
        if k in lineage_mapping["Gunneridae"] or k in lineage_mapping["Magnoliidae"]:
            clade_mapping[k] = "dicots"
        elif k in lineage_mapping["Liliopsida"]:
            clade_mapping[k] = "monocots"
        elif k in lineage_mapping["Acrogymnospermae"]:
            clade_mapping[k] = "conifers"
        else:
            clade_mapping[k] = "rest"

    return clade_mapping


def get_lineage_taxids_from_species(species: str) -> list:
    try:
        taxid = NCBI.get_name_translator([species])[species][0]
        lineage = NCBI.get_lineage(taxid)
        return lineage
    except KeyError:
        return []


def split_lineages(identifiers, plant_species_dict):
    """
    Makes a dictionary of lineage IDs to a set of identifiers belonging in that lineage
    Parameters
    ----------
    identifiers: list of UniProt IDs
    plant_species_dict: dict of UniProt ID to species name

    Returns
    -------
    dict
    """
    lineage_mapping = defaultdict(set)
    for i, identifier in enumerate(identifiers):
        if i % 20 == 0:
            print(i)
        lineage = list(
            NCBI.get_taxid_translator(
                get_lineage_taxids_from_species(plant_species_dict[identifier])
            ).values()
        )
        for lin in lineage:
            lineage_mapping[lin].add(identifier)
    return lineage_mapping
