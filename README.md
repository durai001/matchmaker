## Matchmaker - A joint structure-molecule framework to predict protein-compound specificity

### Dependencies
- modeller
- RDKit
- shap
- XGBoost
- APBS
- PDB2PQR
- geometricus
- caretta

### Code
- `modelling_functions.py` - functions to align protein sequences to templates and prepare for modelling.
- `parallel_model.py`: generate homology models in parallel
- `make_data.py`: functions for extracting features from protein sequences and homology models
- `prediction.py`: functions for training and interpreting Matchmaker classifiers.
